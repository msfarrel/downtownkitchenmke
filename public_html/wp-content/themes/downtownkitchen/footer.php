</div><!-- container -->
<footer class="footer" role="contentinfo">
	<div class="row contact-info">
		<div class="large-10 columns large-centered">
			<div class="row">
				<div class="medium-4 columns contact-entry">
					<?php if( get_field('acf_address', 'option') ): ?>
						<p><span class="icon-map-pin"></span></p>
						<?php the_field('acf_address', 'option'); ?>
					<?php endif; ?>
				</div>
				<div class="medium-4 columns contact-entry">
					<?php if( get_field('acf_contacts', 'option') ): ?>
						<p><span class="icon-phone"></span></p>
						<?php the_field('acf_contacts', 'option'); ?>
					<?php endif; ?>
				</div>

				<div class="medium-4 columns contact-entry">
					<?php if( get_field('acf_hours', 'option') ): ?>
						<p><span class="icon-clock"></span></p>
						<?php the_field('acf_hours', 'option'); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>

	<p class="copyright">&copy; <?php echo date('Y'); ?> <?php the_field('acf_copyright', 'option'); ?></p>

</footer>

<?php // all js scripts are loaded in library/bones.php ?>
<?php wp_footer(); ?>

</body>
</html>