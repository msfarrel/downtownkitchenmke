<?php
/*
 Template Name: Menu Page
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>

	<div class="content" role="main">
		<div class="row">
			<div class="medium-10 medium-centered columns">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>

					<?php 

					// check if the flexible content field has rows of data
					if( have_rows('acf_menu_sections') ): 

					 	// loop through the rows of data
					    while ( have_rows('acf_menu_sections') ) : the_row(); ?>

					    	<div class="menu-section">

					    <?php

							// check current row layout
					        if( get_row_layout() == 'acf_menu_section' ): ?>

					        	<h2 class="menu-section-title"><?php the_sub_field('acf_menu_section_title'); ?></h2>



					        	<?php
								
								$loopcounter = 0; 
								$col = 1;
								
								// check if the nested repeater field has rows of data
					        	if( have_rows('acf_menu_items') ):

								 	// loop through the rows of data
								    while ( have_rows('acf_menu_items') ) : the_row(); ?>

										<?php $loopcounter++; if ($col == 1) echo '<div class="row ">'; ?>

											<div class="medium-6 columns">
												<div class="menu-item">
													<h3 class="menu-title"><?php the_sub_field('acf_menu_item_title'); ?></h3>
													<p><?php the_sub_field('acf_menu_item_desc'); ?></p>
													<p><i><?php the_sub_field('acf_menu_item_price'); ?></i></p>
												</div><!-- menu-item -->
											</div>

										<?php if ($col == 2) echo '</div>'; (($col==2) ? $col=1 : $col++); ?>


									<?php

									endwhile; ?>

										<?php if( $loopcounter%2 ) echo '</div>'; ?>

								<?php 
								
								endif;

					        endif; ?>

					    	</div><!-- menu-section -->

					 <?php 

					 	endwhile; 

					endif; ?>

				<?php endwhile; else : ?>

					<article id="post-not-found" class="hentry cf">
						<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
				
						<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
					</article>

				<?php endif; ?>
				
			</div>
		</div>	
	</div>


<?php get_footer(); ?>
