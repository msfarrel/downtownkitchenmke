<?php
/*
 Template Name: Landing Home Page
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>

	<div class="content" role="main">
		<div class="row">
			<div class="medium-6 columns">
				<div class="intro">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<h1><?php the_title(); ?></h1>
						<?php the_content(); ?>

					<?php endwhile; else : ?>

						<article id="post-not-found" class="hentry cf">
							<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
					
							<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
						</article>

					<?php endif; ?>

					<?php if( get_field('acf_twitter_url') || get_field('acf_twitter_url')): ?>
						<div class="social-links">
						<?php if( get_field('acf_twitter_url') ): ?>
							<p>
								<a href="<?php the_field('acf_twitter_url'); ?>" class="social-link">
									<span class="icon icon-twitter"></span> <?php the_field('acf_twitter_text'); ?>
								</a>
							</p>
						<?php endif; ?>

						<?php if( get_field('acf_facebook_url') ): ?>
							<p>
								<a href="<?php the_field('acf_facebook_url'); ?>" class="social-link">
									<span class="icon icon-facebook"></span> <?php the_field('acf_facebook_text'); ?>
								</a>
							</p>
						<?php endif; ?>
						</div>
					<?php endif; ?>
					
						<div class="newsletter">
							<?php if( get_field('acf_newsletter_title') ): ?>
								<h2 class="newsletter-title"><?php the_field('acf_newsletter_title'); ?></h2>
							<?php endif; ?>
							<?php the_field('acf_newsletter_description'); ?>
							<form action="http://bartolottas.createsend.com/t/r/s/tudhhuy/" method="post">
						        <label for="fieldName">Name</label>
						        <input id="fieldName" name="cm-name" type="text" />
						    
						        <label for="fieldEmail">Email</label>
						        <input id="fieldEmail" name="cm-tudhhuy-tudhhuy" type="email" required />
						    
						        <button type="submit" class="btn btn-default">Subscribe</button>
							</form>
						</div>
					
				
				</div>
			</div>
			<div class="medium-6 columns">
				<div class="specials">
					<?php if( get_field('acf_specials_title') ): ?>
						<h1><?php the_field('acf_specials_title'); ?></h1>
					<?php endif; ?>
					<?php the_field('acf_specials_description'); ?>

					<?php
						// check if the repeater field has rows of data
						if( have_rows('acf_specials') ):

						 	// loop through the rows of data
						    while ( have_rows('acf_specials') ) : the_row(); ?>

						        <div class="special-entry">
									<h2 class="special-title"><?php the_sub_field('acf_special_name'); ?></h2>
									<p><?php the_sub_field('acf_special_description'); ?></p>
									<p><i><?php the_sub_field('acf_special_price'); ?></i></p>
								</div><!-- special-entry -->

						        
					<?php endwhile; endif;?>

					<?php if( get_field('acf_menu_file') ): ?>
						<p><a href="<?php the_field('acf_menu_file'); ?>" class="btn btn-default"><?php the_field('acf_menu_text'); ?></a></p>
					<?php else: ?>
						<p><a href="<?php echo get_page_link(46); ?>" class="btn btn-default"><?php the_field('acf_menu_text'); ?></a></p>
					<?php endif;?>
				</div>
			</div>
		</div>	
	</div>


<?php get_footer(); ?>
