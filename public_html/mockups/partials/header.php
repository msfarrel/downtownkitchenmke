<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->

<head>
	<meta charset="utf-8">

	<!-- Google Chrome Frame for IE -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title><?php if(isset($title)) echo $title; ?></title>

	<meta name="author" content="Savage Solutions - http://savagesolutionsllc.com">
	<meta name="copyright" content="Copyright 2013 Smartfolio">

	<!-- mobile meta (hooray!) -->
	<meta name="HandheldFriendly" content="True">
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, minimal-ui">

	<!-- icons & favicons -->
	<link rel="apple-touch-icon" href="assets/images/icons/apple-icon-touch.png">
	<link rel="icon" href="assets/images/icons/favicon.png">
	<!--[if IE]>
		<link rel="shortcut icon" href="assets/images/icons/favicon.ico">
	<![endif]-->
	<!-- or, set /favicon.ico for IE10 win -->
	<meta name="msapplication-TileColor" content="#f9f9f9">
	<meta name="msapplication-TileImage" content="assets/images/icons/win8-tile-icon.png">

	<link rel="stylesheet" media="screen" href="assets/stylesheets/app.css">

	<!--[if lt IE 9]>
	    <script src="assets/javascripts/vendor/respond.min.js"></script>
	<![endif]-->
    <script src="assets/javascripts/vendor/modernizr.custom.js"></script>
</head>

<body>
	
	<div class="container">
	    <header class="header">
	    
	    </header><!-- header -->
	    
	    <div class="content" role="main">